import React from 'react';
import {Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {addUser, getUsers} from '../../core/redux/actions/user';

class UsersScreen extends React.Component {
  componentDidMount() {
    this.props.getUsers();
  }
  render() {
    return (
      <>
        <Text>Hello Users</Text>
        <Button
          title="Add User"
          onPress={() => {
            this.props.onAddUserClicked({name: 'charles'});
          }}
        />
        <Text>{JSON.stringify(this.props.users)}</Text>
      </>
    );
  }
}
const mapStateToProps = ({users}) => {
  return {
    users: users.users,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddUserClicked: user => {
      dispatch(addUser(user));
    },
    getUsers: () => {
      dispatch(getUsers());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersScreen);
