import React from 'react';
import {Text, Button} from 'react-native';
import {connect} from 'react-redux';

class HomeScreen extends React.Component {
  render() {
    return (
      <>
        <Text>Hello Home</Text>
        <Button
          title="Go To Users"
          onPress={() => {
            this.props.navigation.navigate('Users');
          }}
        />
      </>
    );
  }
}
const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
