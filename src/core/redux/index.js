import {combineReducers} from 'redux';
import users from './reducers/user';

export default combineReducers({
  users,
});
