import {api} from '../index';

export const userService = {
  getUsers() {
    return api.get('/users').then(response => {
      return response.data;
    });
  },
};
